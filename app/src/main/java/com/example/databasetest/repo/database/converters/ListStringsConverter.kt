package com.example.databasetest.repo.database.converters

import androidx.room.TypeConverter

class ListStringsConverter {

    @TypeConverter
    fun fromString(value: String): List<String> {
        return value.split("@")
    }

    @TypeConverter
    fun fromStringArrayList(list: List<String>): String {
        var result = ""
        list.forEach {
            result = "$result@$it"
        }
        return result
    }
}