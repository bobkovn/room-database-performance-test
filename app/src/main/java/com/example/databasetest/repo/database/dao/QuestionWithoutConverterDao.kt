package com.example.databasetest.repo.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.example.databasetest.model.AnswersWithoutConverter
import com.example.databasetest.model.QuestionWithoutConverter

@Dao
abstract class QuestionWithoutConverterDao {

    @Transaction
    open fun saveQuestionWithoutConverter(questions: List<QuestionWithoutConverter>) {
        insertQuestionWithoutConverter(questions)
        questions.forEach { question ->
            question.answers.run {
                for (answer in this) {
                    answer.questionId = question.id
                }
                saveAnswersWithoutConverter(this)
            }
        }
    }

    fun readQuestionWithoutConverter(): List<QuestionWithoutConverter> {
        val result = getQuestionWithoutConverter()
        result.forEach { question ->
            question.answers = getAnswerWithoutConverterById(question.id)
        }
        return result
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertQuestionWithoutConverter(question: List<QuestionWithoutConverter>)

    @Query("SELECT * FROM questionWithoutConverter")
    abstract fun getQuestionWithoutConverter(): List<QuestionWithoutConverter>

    @Query("SELECT * FROM answersWithoutConverter WHERE questionId = :id")
    abstract fun getAnswerWithoutConverterById(id: Long): List<AnswersWithoutConverter>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveAnswersWithoutConverter(answers: List<AnswersWithoutConverter>)
}