package com.example.databasetest.repo

import com.example.databasetest.model.AnswersWithConverter
import com.example.databasetest.model.AnswersWithoutConverter
import com.example.databasetest.model.QuestionWithConverter
import com.example.databasetest.model.QuestionWithoutConverter
import com.example.databasetest.repo.database.AppDataBase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class TestRepository private constructor(
        private val database: AppDataBase
) : CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    companion object {
        @Volatile
        private var INSTANCE: TestRepository? = null

        fun init(
                database: AppDataBase
        ) {
            if (INSTANCE == null) {
                INSTANCE = TestRepository(database)
            }
        }

        fun getInstance(): TestRepository {
            return synchronized(this) {
                INSTANCE?.let {
                    return INSTANCE as TestRepository
                } ?: throw RuntimeException("need to init TestRepository")
            }
        }
    }

    private val questionListWithConverter = arrayListOf<QuestionWithConverter>()
    private val questionListWithoutConverter = arrayListOf<QuestionWithoutConverter>()

    fun prepareData(dataAmount: Int, callback: ((String) -> Unit)? = null) {
        getBenchmark(callback, {
            questionListWithConverter.clear()
            questionListWithoutConverter.clear()
            val answersListWithConverter = arrayListOf<AnswersWithConverter>()
            answersListWithConverter.add(AnswersWithConverter(0, "test", 1))
            answersListWithConverter.add(AnswersWithConverter(1, "test", 2))
            answersListWithConverter.add(AnswersWithConverter(2, "test", 3))
            answersListWithConverter.add(AnswersWithConverter(3, "test", 4))
            answersListWithConverter.add(AnswersWithConverter(4, "test", 5))

            val answersListWithoutConverter = arrayListOf<AnswersWithoutConverter>()
            answersListWithoutConverter.add(AnswersWithoutConverter(0, "test", 1))
            answersListWithoutConverter.add(AnswersWithoutConverter(1, "test", 2))
            answersListWithoutConverter.add(AnswersWithoutConverter(2, "test", 3))
            answersListWithoutConverter.add(AnswersWithoutConverter(3, "test", 4))
            answersListWithoutConverter.add(AnswersWithoutConverter(4, "test", 5))
            for (i in 0 until dataAmount) {
                questionListWithConverter.add(
                        QuestionWithConverter(
                                i.toLong(),
                                "random",
                                2,
                                arrayListOf("test", "test", "test"),
                                answersListWithConverter
                        )
                )

                questionListWithoutConverter.add(
                        QuestionWithoutConverter(
                                i.toLong(),
                                "random",
                                2,
                                arrayListOf("test", "test", "test"),
                                answersListWithoutConverter
                        )
                )
            }

            return@getBenchmark "Finish prepare data: "
        })
    }

    fun saveQuestionWithConverter(callback: ((String) -> Unit)? = null) {
        getBenchmark(callback, {
            database.questionWithConverterDao().saveQuestionWithConverter(questionListWithConverter)
            return@getBenchmark "Save data with converter: "
        })
    }

    fun saveQuestionWithoutConverter(callback: ((String) -> Unit)? = null) {
        getBenchmark(callback, {
            database.questionWithoutConverterDao().saveQuestionWithoutConverter(questionListWithoutConverter)
            return@getBenchmark "Save data without converter: "
        })
    }

    fun readQuestionWithConverter(callback: ((String) -> Unit)? = null) {
        getBenchmark(callback, {
            database.questionWithConverterDao().readQuestionWithConverter()
            return@getBenchmark "Read data with converter: "
        })
    }

    fun readQuestionWithoutConverter(callback: ((String) -> Unit)? = null) {
        getBenchmark(callback, {
            database.questionWithoutConverterDao().readQuestionWithoutConverter()
            return@getBenchmark "Read data without converter: "
        })
    }

    fun clearDatabase(callback: ((String) -> Unit)? = null) {
        getBenchmark(callback, {
            database.clearAllTables()
            return@getBenchmark "Clear database: "
        })
    }

    private fun getBenchmark(callback: ((String) -> Unit)? = null, block: () -> String) {
        launch(Dispatchers.IO) {
            val startTime = System.currentTimeMillis()
            val finishText = block.invoke()
            val endTime = System.currentTimeMillis()
            withContext(Dispatchers.Main) {
                callback?.invoke("$finishText ${endTime - startTime} \n")
            }
        }
    }
}