package com.example.databasetest.repo.database.converters

import androidx.room.TypeConverter
import com.example.databasetest.model.AnswersWithConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class AnswerConverter {
    @TypeConverter
    fun fromStringToAnswers(value: String): List<AnswersWithConverter> {
        val answers = value.split("#")
        val list = arrayListOf<AnswersWithConverter>()
        answers.forEach {
            if (it.isNotEmpty()) {
                val answer = it.split("@")
                list.add(AnswersWithConverter(answer[0].toLong(), answer[2], answer[1].toLong()))
            }
        }
        return list
    }

    @TypeConverter
    fun fromAnswersArrayList(list: List<AnswersWithConverter>): String {
        var result = ""
        list.forEach {
            result = "$result${it.id}@${it.questionId}@${it.title}#"
        }
        return result
    }
}