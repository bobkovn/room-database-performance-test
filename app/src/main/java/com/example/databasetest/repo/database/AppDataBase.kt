package com.example.databasetest.repo.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.databasetest.repo.database.converters.AnswerConverter
import com.example.databasetest.repo.database.dao.QuestionWithConverterDao
import com.example.databasetest.repo.database.dao.QuestionWithoutConverterDao
import com.example.databasetest.model.AnswersWithoutConverter
import com.example.databasetest.model.QuestionWithConverter
import com.example.databasetest.model.QuestionWithoutConverter
import com.example.databasetest.repo.database.converters.ListStringsConverter

@Database( entities = [
    QuestionWithConverter::class,
    QuestionWithoutConverter::class,
    AnswersWithoutConverter::class],
    version = 1,
    exportSchema = false)
@TypeConverters(AnswerConverter::class, ListStringsConverter::class)
abstract class AppDataBase : RoomDatabase() {
    companion object {
        @Volatile
        private var INSTANCE: AppDataBase? = null

        fun getInstance(
            context: Context
        ): AppDataBase {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: Room.databaseBuilder(
                    context,
                    AppDataBase::class.java,
                    "database-app"
                ).fallbackToDestructiveMigration().build()
                    .also { INSTANCE = it }
            }
        }
    }


    abstract fun questionWithoutConverterDao(): QuestionWithoutConverterDao
    abstract fun questionWithConverterDao(): QuestionWithConverterDao
}