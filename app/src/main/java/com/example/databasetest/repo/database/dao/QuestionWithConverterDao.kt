package com.example.databasetest.repo.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.databasetest.model.QuestionWithConverter

@Dao
abstract class QuestionWithConverterDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveQuestionWithConverter(questions: List<QuestionWithConverter>)

    @Query("SELECT * FROM questionWithConverter")
    abstract fun readQuestionWithConverter(): List<QuestionWithConverter>
}