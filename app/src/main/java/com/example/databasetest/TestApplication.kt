package com.example.databasetest

import android.app.Application
import com.example.databasetest.repo.TestRepository
import com.example.databasetest.repo.database.AppDataBase

class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        TestRepository.init(AppDataBase.getInstance(this))
    }
}