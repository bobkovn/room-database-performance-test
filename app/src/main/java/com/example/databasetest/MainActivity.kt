package com.example.databasetest

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.databasetest.repo.TestRepository

const val RESULT_COUNT = 2

class MainActivity : AppCompatActivity() {
    private lateinit var resultTextView: TextView
    private lateinit var amountDataEditText: EditText
    private lateinit var prepareDataButton: Button
    private lateinit var startTestButton: Button
    private lateinit var clearDatabaseCheckBox: CheckBox
    private lateinit var savePreviousDataCheckBox: CheckBox
    private lateinit var progressBar: ProgressBar
    private lateinit var repository: TestRepository
    private var resultCount = RESULT_COUNT
    private var needDatabaseClear = false
    private var needSavePreviousData = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        repository = TestRepository.getInstance()
        resultTextView = findViewById(R.id.resultTextView)
        amountDataEditText = findViewById(R.id.dataAmountEditText)
        progressBar = findViewById(R.id.progressBar)
        startTestButton = findViewById(R.id.startTestButton)
        prepareDataButton = findViewById(R.id.prepareDataButton)
        clearDatabaseCheckBox = findViewById(R.id.clearDatabaseCheckbox)
        savePreviousDataCheckBox = findViewById(R.id.savePreviousDataCheckbox)
        repository.prepareData(10_000)

        prepareDataButton.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            prepareDataButton.isEnabled = false
            repository.prepareData(amountDataEditText.text.toString().toInt()) { data ->
                Toast.makeText(this, data, Toast.LENGTH_LONG).show()
                progressBar.visibility = View.GONE
                prepareDataButton.isEnabled = true
            }
        }

        startTestButton.setOnClickListener {
            startTest()
        }

        clearDatabaseCheckBox.setOnCheckedChangeListener { _, isChecked ->
            needDatabaseClear = isChecked
        }

        savePreviousDataCheckBox.setOnCheckedChangeListener { _, isChecked ->
            needSavePreviousData = isChecked
        }
    }

    private fun startTest() {
        if (needSavePreviousData) {
            setResultText("---------------------\n")
        } else {
            resultTextView.text = ""
        }
        resultCount = RESULT_COUNT
        progressBar.visibility = View.VISIBLE
        repository.saveQuestionWithConverter { saveResult ->
            setResultText(saveResult)
            repository.readQuestionWithConverter { readResult ->
                resultCount--
                checkAllResult()
                setResultText(readResult)
            }
        }
        repository.saveQuestionWithoutConverter { saveResult ->
            setResultText(saveResult)
            repository.readQuestionWithoutConverter { readResult ->
                resultCount--
                checkAllResult()
                setResultText(readResult)
            }
        }
    }

    private fun checkAllResult() {
        if (resultCount == 0) {
            progressBar.visibility = View.INVISIBLE
            if (needDatabaseClear) {
                repository.clearDatabase { clearResult ->
                    setResultText(clearResult)
                }
            }
        }
    }

    private fun setResultText(result: String) {
        val previousText = resultTextView.text
        resultTextView.text = "$previousText $result"
    }

}
