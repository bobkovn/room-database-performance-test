package com.example.databasetest.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "answersWithoutConverter",
    indices = [Index(value = ["questionId"])],
    foreignKeys = [ForeignKey(
        entity = QuestionWithoutConverter::class,
        parentColumns = ["id"],
        childColumns = ["questionId"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class AnswersWithoutConverter(
    @PrimaryKey var id: Long,
    var title: String,
    var questionId: Long
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString() ?: "",
        parcel.readLong()
    )

    constructor() : this(0, "", 0)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(title)
        parcel.writeLong(questionId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AnswersWithoutConverter> {
        override fun createFromParcel(parcel: Parcel): AnswersWithoutConverter {
            return AnswersWithoutConverter(parcel)
        }

        override fun newArray(size: Int): Array<AnswersWithoutConverter?> {
            return arrayOfNulls(size)
        }
    }
}