package com.example.databasetest.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "questionWithoutConverter")
data class QuestionWithoutConverter(
    @PrimaryKey
    var id: Long,
    var title: String,
    var answerType: Int,
    var images: List<String>,
    @Ignore var answers: List<AnswersWithoutConverter>
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.createStringArrayList() ?: listOf(),
        parcel.createTypedArrayList(AnswersWithoutConverter) ?: arrayListOf<AnswersWithoutConverter>()
    )

    constructor() : this(0, "", 0, listOf(), arrayListOf<AnswersWithoutConverter>())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(title)
        parcel.writeInt(answerType)
        parcel.writeStringList(images)
        parcel.writeTypedList(answers)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QuestionWithoutConverter> {
        override fun createFromParcel(parcel: Parcel): QuestionWithoutConverter {
            return QuestionWithoutConverter(parcel)
        }

        override fun newArray(size: Int): Array<QuestionWithoutConverter?> {
            return arrayOfNulls(size)
        }
    }
}