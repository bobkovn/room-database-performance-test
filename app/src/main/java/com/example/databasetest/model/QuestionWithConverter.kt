package com.example.databasetest.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "questionWithConverter")
data class QuestionWithConverter(
    @PrimaryKey
    var id: Long,
    var title: String,
    var answerType: Int,
    var images: List<String>,
    var answers: List<AnswersWithConverter>
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.createStringArrayList() ?: listOf(),
        parcel.createTypedArrayList(AnswersWithConverter) ?: arrayListOf<AnswersWithConverter>()
    )

    constructor() : this(0, "", 0, listOf(), arrayListOf<AnswersWithConverter>())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(title)
        parcel.writeInt(answerType)
        parcel.writeStringList(images)
        parcel.writeTypedList(answers)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QuestionWithConverter> {
        override fun createFromParcel(parcel: Parcel): QuestionWithConverter {
            return QuestionWithConverter(parcel)
        }

        override fun newArray(size: Int): Array<QuestionWithConverter?> {
            return arrayOfNulls(size)
        }
    }
}