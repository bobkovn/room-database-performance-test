package com.example.databasetest.model

import android.os.Parcel
import android.os.Parcelable

data class AnswersWithConverter(
    var id: Long,
    var title: String,
    var questionId: Long
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString() ?: "",
        parcel.readLong()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(title)
        parcel.writeLong(questionId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AnswersWithConverter> {
        override fun createFromParcel(parcel: Parcel): AnswersWithConverter {
            return AnswersWithConverter(parcel)
        }

        override fun newArray(size: Int): Array<AnswersWithConverter?> {
            return arrayOfNulls(size)
        }
    }
}